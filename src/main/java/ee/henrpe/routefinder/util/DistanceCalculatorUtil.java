package ee.henrpe.routefinder.util;

import ee.henrpe.routefinder.domain.Airport;
import lombok.experimental.UtilityClass;
import org.apache.lucene.util.SloppyMath;

@UtilityClass
public class DistanceCalculatorUtil {
  public Double getMetresBetweenAirports(Airport sourceAirport, Airport destinationAirport) {
    return SloppyMath.haversinMeters(
      sourceAirport.getLatitude(),
      sourceAirport.getLongitude(),
      destinationAirport.getLatitude(),
      destinationAirport.getLongitude()
    );
  }
}
