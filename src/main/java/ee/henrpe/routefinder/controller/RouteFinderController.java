package ee.henrpe.routefinder.controller;

import ee.henrpe.routefinder.domain.FindRouteRequest;
import ee.henrpe.routefinder.domain.FindRouteResponse;
import ee.henrpe.routefinder.service.RouteFinderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class RouteFinderController {
  private final RouteFinderService routeFinderService;

  @PostMapping(path = "/find-route", consumes = "application/json", produces = "application/json")
  public ResponseEntity<FindRouteResponse> findRoute(@Valid @RequestBody FindRouteRequest findRouteRequest) {
    return new ResponseEntity<>(routeFinderService.findRoute(findRouteRequest), HttpStatus.OK);
  }
}
