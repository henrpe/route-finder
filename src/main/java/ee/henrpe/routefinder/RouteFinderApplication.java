package ee.henrpe.routefinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class RouteFinderApplication {
	public static void main(String[] args) {
		SpringApplication.run(RouteFinderApplication.class, args);
	}
}
