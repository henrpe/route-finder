package ee.henrpe.routefinder.service;

import ee.henrpe.routefinder.domain.Airport;
import ee.henrpe.routefinder.domain.FindRouteRequest;
import ee.henrpe.routefinder.domain.FindRouteResponse;
import ee.henrpe.routefinder.domain.Flight;
import ee.henrpe.routefinder.util.DistanceCalculatorUtil;
import lombok.RequiredArgsConstructor;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.BellmanFordShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@Service
@RequiredArgsConstructor
public class RouteFinderService {
  private static final int EPSILON = 2;
  private static final int MAX_HOPS = 4;

  private final DataService dataService;

  @Cacheable("findRouteCache")
  public FindRouteResponse findRoute(FindRouteRequest findRouteRequest) {
    List<String> sourceAndDestinationAirportIds = getSourceAndDestinationAirportIds(findRouteRequest);
    return getShortestPathFromSourceToDestination(sourceAndDestinationAirportIds.get(0), sourceAndDestinationAirportIds.get(1));
  }

  private List<String> getSourceAndDestinationAirportIds(FindRouteRequest findRouteRequest) {
    switch (findRouteRequest.getAirportCodeType()) {
      case IATA:
        return asList(
          dataService.getIataToIdMap().get(findRouteRequest.getSourceAirportCode()),
          dataService.getIataToIdMap().get(findRouteRequest.getDestinationAirportCode())
        );
      case ICAO:
        return asList(
          dataService.getIcaoToIdMap().get(findRouteRequest.getSourceAirportCode()),
          dataService.getIcaoToIdMap().get(findRouteRequest.getDestinationAirportCode())
        );
      default:
        throw new RuntimeException("Invalid airport code type!");
    }
  }

  private FindRouteResponse getShortestPathFromSourceToDestination(String sourceAirportId, String destinationAirportId) {
    BellmanFordShortestPath<String, DefaultWeightedEdge> shortestPath =
      new BellmanFordShortestPath<>(dataService.getGraph(), EPSILON, MAX_HOPS);

    GraphPath<String, DefaultWeightedEdge> path;
    try {
      path = shortestPath.getPath(sourceAirportId, destinationAirportId);
      if (path == null) {
        return new FindRouteResponse("No route available between airports!");
      }
    } catch (IllegalArgumentException e) {
      return new FindRouteResponse(getErrorMessage(e.getMessage()));
    }

    List<Flight> flights = new ArrayList<>();
    Double totalDistanceInMetres = 0D;
    for (int i = 0; i < path.getVertexList().size() - 1; i++) {
      Airport sourceAirport = dataService.getIdToAirportMap().get(path.getVertexList().get(i));
      Airport destinationAirport = dataService.getIdToAirportMap().get(path.getVertexList().get(i + 1));
      Double metresBetweenAirports = DistanceCalculatorUtil.getMetresBetweenAirports(sourceAirport, destinationAirport);
      flights.add(new Flight(sourceAirport, destinationAirport, metresBetweenAirports));
      totalDistanceInMetres += metresBetweenAirports;
    }
    return new FindRouteResponse(flights, totalDistanceInMetres, "Optimal route successfully found!");
  }

  private String getErrorMessage(String errorMessage) {
    if ("Graph must contain the source vertex!".equals(errorMessage)) {
      errorMessage = "Source airport was not found!";
    } else if ("Graph must contain the sink vertex!".equals(errorMessage)) {
      errorMessage = "Destination airport was not found!";
    }
    return errorMessage;
  }
}
