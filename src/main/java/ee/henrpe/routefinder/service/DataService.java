package ee.henrpe.routefinder.service;

import ee.henrpe.routefinder.domain.Airport;
import ee.henrpe.routefinder.util.DistanceCalculatorUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Service
public class DataService {
  private static final String AIRPORTS_FILE_NAME = "airports.dat";
  private static final String ROUTES_FILE_NAME = "routes.dat";

  @Getter
  private final Map<String, Airport> idToAirportMap = new HashMap<>();
  @Getter
  private final Map<String, String> iataToIdMap = new HashMap<>();
  @Getter
  private final Map<String, String> icaoToIdMap = new HashMap<>();
  @Getter
  private final SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph =
    new SimpleDirectedWeightedGraph<>(DefaultWeightedEdge.class);

  public DataService() {
    populateAirportData();
    populateGraph();
  }

  @SneakyThrows
  private void populateAirportData() {
    InputStream airportsInputStream = new ClassPathResource(AIRPORTS_FILE_NAME).getInputStream();
    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(airportsInputStream))) {
      for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
        putAirportLineDataToMaps(line);
      }
    }
  }

  @SneakyThrows
  private void populateGraph() {
    InputStream routesInputStream = new ClassPathResource(ROUTES_FILE_NAME).getInputStream();
    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(routesInputStream))) {
      for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
        addRouteToGraph(line);
      }
    }
  }

  private void putAirportLineDataToMaps(String line) {
    String[] splitLine = line.split(",");
    String id = splitLine[0];
    String name = splitLine[1].replaceAll("\"", "");
    String iata = "\\N".equals(splitLine[4]) ? null : splitLine[4].replaceAll("\"", "");
    String icao = splitLine[5].replaceAll("\"", "");
    double latitude = Double.parseDouble(splitLine[6]);
    double longitude = Double.parseDouble(splitLine[7]);
    idToAirportMap.put(id, new Airport(name, iata, icao, latitude, longitude));
    iataToIdMap.put(iata, id);
    icaoToIdMap.put(icao, id);
  }

  private void addRouteToGraph(String line) {
    String[] splitLine = line.split(",");
    String sourceAirportId = splitLine[3];
    String destinationAirportId = splitLine[5];
    Airport sourceAirport = idToAirportMap.get(sourceAirportId);
    Airport destinationAirport = idToAirportMap.get(destinationAirportId);

    if (sourceAirport == null || destinationAirport == null) {
      return;
    }

    if (!graph.containsVertex(sourceAirportId)) {
      graph.addVertex(sourceAirportId);
    }
    if (!graph.containsVertex(destinationAirportId)) {
      graph.addVertex(destinationAirportId);
    }

    DefaultWeightedEdge weightedEdge = graph.addEdge(sourceAirportId, destinationAirportId);
    if (weightedEdge != null) {
      graph.setEdgeWeight(
        weightedEdge,
        DistanceCalculatorUtil.getMetresBetweenAirports(
          sourceAirport,
          destinationAirport
        )
      );
    }
  }
}
