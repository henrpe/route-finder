package ee.henrpe.routefinder.domain;

public enum AirportCodeType {
  IATA,
  ICAO
}
