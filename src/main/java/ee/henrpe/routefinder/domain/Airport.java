package ee.henrpe.routefinder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Airport {
  private final String name;
  private final String iata;
  private final String icao;
  private final Double latitude;
  private final Double longitude;
}
