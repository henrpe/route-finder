package ee.henrpe.routefinder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Flight {
  private final Airport sourceAirport;
  private final Airport destinationAirport;
  private final Double metresBetweenAirports;
}
