package ee.henrpe.routefinder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public class FindRouteResponse {
  private List<Flight> flights;
  private Double totalDistanceInMetres;
  private final String message;
}
