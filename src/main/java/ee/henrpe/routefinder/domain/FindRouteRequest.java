package ee.henrpe.routefinder.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@EqualsAndHashCode
public class FindRouteRequest {
  @NotBlank(message = "Source airport code is missing from request body")
  private String sourceAirportCode;
  @NotBlank(message = "Destination airport code is missing from request body")
  private String destinationAirportCode;
  private AirportCodeType airportCodeType;
}
