package ee.henrpe.routefinder;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.web.servlet.MockMvc;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.StreamUtils.copyToString;

@SpringBootTest
@AutoConfigureMockMvc
class RouteFinderApplicationTests {
  @Autowired
  private MockMvc mockMvc;

  @Test
  void shouldReturnOptimalRoute() throws Exception {
    String expectedResponse = copyToString(new ClassPathResource("TLL-LPL_route.json").getInputStream(), UTF_8);
    mockMvc.perform(post("/find-route")
      .contentType("application/json")
      .content("{\"sourceAirportCode\":\"TLL\", \"destinationAirportCode\":\"LPL\", \"airportCodeType\":\"IATA\"}"))
      .andExpect(status().isOk())
      .andExpect(content().json(expectedResponse));
  }

  @Test
  void shouldReturnNotFoundIfSourceAirportIsMissing() throws Exception {
    String expected = copyToString(new ClassPathResource("source_airport_missing.json").getInputStream(), UTF_8);
    mockMvc.perform(post("/find-route")
      .contentType("application/json")
      .content("{\"sourceAirportCode\":\"missing\", \"destinationAirportCode\":\"LPL\", \"airportCodeType\":\"IATA\"}"))
      .andExpect(status().isOk())
      .andExpect(content().json(expected));
  }

  @Test
  void shouldReturnNotFoundIfDestinationAirportIsMissing() throws Exception {
    String expected = copyToString(new ClassPathResource("destination_airport_missing.json").getInputStream(), UTF_8);
    mockMvc.perform(post("/find-route")
      .contentType("application/json")
      .content("{\"sourceAirportCode\":\"TLL\", \"destinationAirportCode\":\"missing\", \"airportCodeType\":\"IATA\"}"))
      .andExpect(status().isOk())
      .andExpect(content().json(expected));
  }

  @Test
  void shouldReturnErrorIfAirportCodeTypeIsInvalid() throws Exception {
    String expected = copyToString(new ClassPathResource("airport_code_type_invalid.txt").getInputStream(), UTF_8);
    mockMvc.perform(post("/find-route")
      .contentType("application/json")
      .content("{\"sourceAirportCode\":\"TLL\", \"destinationAirportCode\":\"LPL\", \"airportCodeType\":\"invalid\"}"))
      .andExpect(status().isInternalServerError())
      .andExpect(content().string(expected));
  }

  @Test
  void shouldReturnErrorIfSourceAirportInvalid() throws Exception {
    String expected = copyToString(new ClassPathResource("source_airport_invalid.txt").getInputStream(), UTF_8);
    mockMvc.perform(post("/find-route")
      .contentType("application/json")
      .content("{\"destinationAirportCode\":\"LPL\", \"airportCodeType\":\"ICAO\"}"))
      .andExpect(status().isInternalServerError())
      .andExpect(content().string(expected));
  }

  @Test
  void shouldReturnErrorIfDestinationAirportInvalid() throws Exception {
    String expected = copyToString(new ClassPathResource("destination_airport_invalid.txt").getInputStream(), UTF_8);
    mockMvc.perform(post("/find-route")
      .contentType("application/json")
      .content("{\"sourceAirportCode\":\"TLL\", \"airportCodeType\":\"IATA\"}"))
      .andExpect(status().isInternalServerError())
      .andExpect(content().string(expected));
  }
}
