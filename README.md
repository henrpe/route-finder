# route-finder

A JSON REST API to find optimal plane routes between airports.

## Requirements
Java SDK 11

## Running

Execute `./gradlew bootRun` (macOS/linux) or `gradlew bootRun` (windows) in project root directory.

## Building bootable Jar

Execute `./gradlew build` (macOS/linux) or `gradlew build` (windows) in project root directory.

## Using application

### Swagger

After application has been started navigate to http://localhost:8080/swagger-ui.html 

### Curl

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"sourceAirportCode":"TLL", "destinationAirportCode":"LPL", "airportCodeType": "IATA"}' \
  http://localhost:8080/find-route
```

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"sourceAirportCode":"EETN", "destinationAirportCode":"EGGP", "airportCodeType": "ICAO"}' \
  http://localhost:8080/find-route
```

